import 'package:travelll/models/list_result_model.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;
import 'dart:async';

class Services {
  static const String url = 'hotels4.p.rapidapi.com';
  static Future<List<ListResult>> getListResult() async {
    final queryParameters = {
      "currency": "USD",
      "locale": "en_US",
      "sortOrder": "PRICE",
      "destinationId": "1506246",
      "pageNumber": "1",
      "checkIn": "2020-01-08",
      "checkOut": "2020-01-15",
      "pageSize": "25",
      "adults1": "1"
    };
    final uri = Uri.https(url, '/properties/list', queryParameters);
    final headers = {
      "x-rapidapi-key": "737fa5eadbmsh35dcdba96f07770p1b285bjsn72dc2023167f"
    };
    final response = await http.get(uri, headers: headers);

    if (response.statusCode == 200) {
      List<ListResult> list = parseListResults(response.body);
      return list;
    } else {
      print('Request failed with status: ${response.statusCode}.');
    }
  }

  static List<ListResult> parseListResults(String responseBody) {
    final jsonResponse = convert.jsonDecode(responseBody);
    final results = jsonResponse['data']['body']['searchResults']['results'];
    final parsed = results.cast<Map<String, dynamic>>();
    return parsed.map<ListResult>((json) => ListResult.fromJson(json)).toList();
  }

  static Future<List> getData(String query) async {
    final queryParameters = {'query': query};
    final uri = Uri.https(url, '/locations/search', queryParameters);
    final headers = {
      "x-rapidapi-key": "737fa5eadbmsh35dcdba96f07770p1b285bjsn72dc2023167f"
    };
    final response = await http.get(uri, headers: headers);

    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      var suggestions = jsonResponse['suggestions'];
      return suggestions;
    } else {
      print('Request failed with status: ${response.statusCode}.');
    }
  }
}
