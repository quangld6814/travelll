import 'package:flutter/material.dart';
import 'package:flutter_html/style.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_html/flutter_html.dart';
import 'dart:convert' as convert;
import 'dart:async';
import 'detail_list_screen.dart';

class SearchScreen extends SearchDelegate<String> {
  List data = List();
  final url = 'hotels4.p.rapidapi.com';
  Future<List> getData(String query) async {
    final queryParameters = {'query': query};
    final uri = Uri.https(url, '/locations/search', queryParameters);
    final headers = {
      "x-rapidapi-key": "737fa5eadbmsh35dcdba96f07770p1b285bjsn72dc2023167f"
    };
    final response = await http.get(uri, headers: headers);

    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      var suggestions = jsonResponse['suggestions'];
      data = suggestions;
    } else {
      print('Request failed with status: ${response.statusCode}.');
    }
  }

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = "";
        },
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    // TODO: implement buildLeading
    return IconButton(
        icon: AnimatedIcon(
            icon: AnimatedIcons.menu_arrow, progress: transitionAnimation),
        onPressed: () {
          close(context, null);
        });
  }

  @override
  Widget buildResults(BuildContext context) {}

  @override
  Widget buildSuggestions(BuildContext context) {
    var suggestionList = [];
    if (query.isNotEmpty) {
      getData(query);
      suggestionList = data;
    }

    return ListView.builder(
        shrinkWrap: true,
        physics: PageScrollPhysics(),
        itemBuilder: (context, index) => Padding(
          padding: EdgeInsets.all(0.0),
          child: Column(
            children: <Widget>[
              Text(
                suggestionList[index]['group'],
              ),
              ListView.builder(
                  shrinkWrap: true,
                  physics: PageScrollPhysics(),
                  itemBuilder: (context, i) => ListTile(
                    leading: Icon(Icons.location_on, size: 18),
                    title: Html(
                      data: suggestionList[index]['entities'][i]['caption'],
                      style: {
                        ".highlighted": Style(
                          fontWeight: FontWeight.bold,
                        ),
                      },
                    ),
                    onTap: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) =>
                            DetailListScreen(
                            destinationId: suggestionList[index]['entities'][i]['destinationId'], caption: suggestionList[index]['entities'][i]['caption']
                        ),
                      ),
                    ),
                  ),
                  itemCount: suggestionList[index]['entities'].length)
            ],
          ),
        ),
        itemCount: suggestionList.length);
  }
}
