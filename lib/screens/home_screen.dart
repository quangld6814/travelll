import 'package:flutter/material.dart';
import 'package:travelll/animation/fade_animation.dart';
import 'package:travelll/screens/search_screen.dart';
import 'package:travelll/widgets/destination_carousel.dart';
import 'package:travelll/widgets/hotel_carousel.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  var url = 'https://hotels4.p.rapidapi.com/locations/search?query=hanoi';

  Future<String> getData() async {
    http.Response response = await http.get(
        Uri.encodeFull(url),
        headers: {
          "Accept": "application/json",
          "x-rapidapi-key": "2e64e4fc55msh4e40dcb8142bb2ap1c0a64jsnd15508775294"
        }
    );
    Map data = jsonDecode(response.body);
    List group = [];
    data['suggestions'].forEach((e) => group.add(e['group']));
    print(group);
  }
//  int _selectedIndex = 0;
  int _currentTab = 0;
//  List<IconData> _icons = [
//    FontAwesomeIcons.plane,
//    FontAwesomeIcons.bed,
//    FontAwesomeIcons.walking,
//    FontAwesomeIcons.biking,
//  ];

//  Widget _buildIcon(int index) {
//    return GestureDetector(
//      onTap: () {
//        setState(() {
//          _selectedIndex = index;
//        });
//      },
//      child: Container(
//        height: 60.0,
//        width: 60.0,
//        decoration: BoxDecoration(
//          color: _selectedIndex == index
//              ? Theme.of(context).accentColor
//              : Color(0xFFE7EBEE),
//          borderRadius: BorderRadius.circular(30.0),
//        ),
//        child: Icon(
//          _icons[index],
//          size: 25.0,
//          color: _selectedIndex == index
//              ? Theme.of(context).primaryColor
//              : Color(0xFFB4C1C4),
//        ),
//      ),
//    );
//  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: ListView(
          children: <Widget>[
            Container(
              height: 300,
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage('assets/images/venice.jpg'),
                    fit: BoxFit.cover),
              ),
              child: Container(
                decoration: BoxDecoration(
                    gradient:
                        LinearGradient(begin: Alignment.bottomRight, colors: [
                  Colors.black.withOpacity(.8),
                  Colors.black.withOpacity(.2),
                ])),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    FadeAnimation(1,
                        Text(
                          "What you would like to find?",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 40,
                              fontWeight: FontWeight.bold),
                        )),
                    SizedBox(
                      height: 30,
                    ),
                    GestureDetector(
                      onTap: () {
                        showSearch(
                            context: context, delegate: SearchScreen());
                      },
                      child: FadeAnimation(1.3,
                          Container(
                            padding: EdgeInsets.symmetric(vertical: 3, horizontal: 15),
                            margin: EdgeInsets.symmetric(horizontal: 40),
                            height: 50,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                              color: Colors.white,
                            ),
                            child: Row(
                                children: <Widget>[
                                  Icon(
                                    Icons.search,
                                    color: Colors.grey,
                                  ),
                                  Text("  Try search \'Ha Noi\'",
                                    style: TextStyle(color: Colors.grey))
                                ]
                            ),
                          )),
                    ),
                    SizedBox(
                      height: 30,
                    )
                  ],
                ),
              ),
            ),
            SizedBox(height: 20.0),
            DestinationCarousel(),
            SizedBox(height: 20.0),
            HotelCarousel(),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentTab,
        onTap: (int value) {
          setState(() {
            _currentTab = value;
          });
        },
        items: [
          BottomNavigationBarItem(
            icon: Icon(
              Icons.search,
              size: 30.0,
            ),
            title: SizedBox.shrink(),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.local_pizza,
              size: 30.0,
            ),
            title: SizedBox.shrink(),
          ),
          BottomNavigationBarItem(
            icon: CircleAvatar(
              radius: 15.0,
              backgroundImage: NetworkImage('http://i.imgur.com/zL4Krbz.jpg'),
            ),
            title: SizedBox.shrink(),
          )
        ],
      ),
    );
  }
}
