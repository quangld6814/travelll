import 'package:flutter/material.dart';
import 'package:travelll/models/list_result_model.dart';
import 'package:travelll/service/services.dart';

class DetailListScreen extends StatefulWidget {
  final String destinationId;
  final String caption;

  DetailListScreen({this.destinationId, this.caption});

  @override
  _DetailListScreenState createState() => _DetailListScreenState();
}

class _DetailListScreenState extends State<DetailListScreen> {
  Text _buildRatingStars(double rating) {
    String stars = '';
    for (int i = 0; i < rating; i++) {
      stars += '⭐ ';
    }
    stars.trim();
    return Text(stars);
  }

  List<ListResult> datas = List();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Services.getListResult().then((listFromServer) {
      setState(() {
        datas = listFromServer;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Expanded(
            child: ListView.builder(
              padding: EdgeInsets.only(top: 10.0, bottom: 15.0),
              itemCount: datas.length,
              itemBuilder: (BuildContext context, int index) {
                ListResult data = datas[index];
                return Stack(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.fromLTRB(40.0, 5.0, 20.0, 5.0),
                      height: 170.0,
                      width: double.infinity,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(100.0, 20.0, 20.0, 20.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  width: 120.0,
                                  child: Text(
                                    data.name,
                                    style: TextStyle(
                                      fontSize: 18.0,
                                      fontWeight: FontWeight.w600,
                                    ),
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: 2,
                                  ),
                                ),
                              ],
                            ),
                            Text(
                              data.providerType,
                              style: TextStyle(
                                color: Colors.grey,
                              ),
                            ),
                            _buildRatingStars(data.starRating),
                            SizedBox(height: 10.0)
                          ],
                        ),
                      ),
                    ),
                    Positioned(
                      left: 20.0,
                      top: 15.0,
                      bottom: 15.0,
                      child: ClipRRect(
                          borderRadius: BorderRadius.circular(10.0),
                          child: Image(
                            width: 110.0,
                            image: NetworkImage(
                              data.thumbnailUrl,
                            ),
                            fit: BoxFit.cover,
                          )),
                    ),
                  ],
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
