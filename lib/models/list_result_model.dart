class ListResult {
  int id;
  String name;
  String thumbnailUrl;
  double starRating;
  String providerType;

  ListResult({
    this.id,
    this.name,
    this.thumbnailUrl,
    this.starRating,
    this.providerType
  });

  factory ListResult.fromJson(Map<String, dynamic> json) {
    return ListResult(
      id: json['id'] as int,
      name: json['name'] as String,
      thumbnailUrl: json['thumbnailUrl'] as String,
      starRating: json['starRating'] as double,
      providerType: json['providerType'] as String
    );
  }
}
